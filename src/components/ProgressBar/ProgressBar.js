/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants';
import VisuallyHidden from '../VisuallyHidden';


/**
 * The story behind the border-radius for large
 * 
 * Concentric circles
 *
 * When we nest elements that use border-radius, we need to tweak the outer element to be more curved than the inner one.
 *
 * The formula to work out the correct radius is:
 *
 * `outer_radius = inner_radius + (gap_between_elements / 2)`
 *
 * Some folks prefer to solve this visually rather than using a formula, but if you find that you can't tell which number is best, the formula can help!
 *
 * Here's a codepen that shows the formula at work. It uses calc, which we haven't seen yet, but hopefully the idea is clear regardless.
 * https://codepen.io/joshwcomeau/pen/ExgeyPK
 */
const SIZES = {
  small: {
    height: 8,
    padding: 0,
    radius: 4,
  },
  medium: {
    height: 12,
    padding: 0,
    radius: 4,
  },
  large: {
    height: 16,
    padding: 4,
    radius: 8,
  }
};

const ProgressBar = ({ value, size }) => {
  const styles = SIZES[size];

  console.log(styles)

  if(!styles) {
    throw new Error(`Unknown size passed to ProgressBar: ${size}`)
  }

  return <Wrapper 
    role="progressbar"
    aria-valuenow={size}
    aria-valuemin="0"
    aria-valuemax="100"
    style={{
      '--padding': `${styles.padding}px`,
      '--radius': `${styles.radius}px`
    }}
  >
    <VisuallyHidden>{value}</VisuallyHidden>
    <BarWrapper>
      <Bar 
        style={{ 
          '--width': `${value}%`,
          '--height': `${styles.height}px`
        }} 
      />
    </BarWrapper>
  </Wrapper>;
};

const Wrapper = styled.div`
  background-color: ${COLORS.transparentGray15};
  box-shadow: inset 0 2px 4px ${COLORS.transparentGray35};
  border-radius: var(--radius);
  padding: var(--padding);
`

const BarWrapper = styled.div`
  border-radius: 4px;

  /* Trim off corners when progress bar is near full */
  overflow: hidden;
`

const Bar = styled.div`
  width: var(--width);
  height: var(--height);
  background-color: ${COLORS.primary};
  border-radius: 4px 0 0 4px;
`

export default ProgressBar;
